
import './App.css';
import AppNavBar from './components/appNavBar.js'
import {BrowserRouter as Router} from 'react-router-dom'
import { UserProvider } from './UserContext';
import { useEffect, useState } from 'react';
import AnimatedRoutes from './components/AnimatedRoutes';
function App() {
  const [user,setUser] = useState(localStorage.getItem({id:null,isAdmin:null}))
  const unSetUser = () =>{
    localStorage.clear();
  }
  useEffect(()=> {
  },[user])
  return (
    <div className='background dosis'>
      <UserProvider value={{user,setUser,unSetUser}} >
      
        <Router >
          <AppNavBar/>
          <AnimatedRoutes/>
          
        </Router>
      </UserProvider>
    </div>
    
  );
}

export default App;
