import React from 'react'
import Home from '../pages/Home';
import Services from '../pages/Services';
import Register from '../pages/Register';
import Login from '../pages/Login'
import Logout from '../pages/Logout';
import NotFound from '../components/NotFound';
import ServiceView from '../components/ServiceView';
import {Route, Routes, useLocation} from 'react-router-dom'
import {AnimatePresence} from 'framer-motion'
import AdminDashboard from '../pages/AdminDashboard';
import UserDashboard from '../pages/UserDashboard';
function AnimatedRoutes() {
    const location = useLocation();
  return (
    <AnimatePresence>
        <Routes location={location} key={location.pathname}>
            <Route path ='/' element={<Home/>}/>
            <Route path ='/Services' element={<Services/>}/>
            <Route path ='/Login' element={<Login/>}/>
            <Route path ='/Register' element={<Register/>}/>
            <Route path ='/Logout' element={<Logout/>}/>
            <Route path='*' element={<NotFound />}/>
            <Route path='/service/:serviceId' element={<ServiceView/>}/>
            <Route path='/AdminDashboard' element={<AdminDashboard/>}/>
            <Route path='/UserDashboard' element={<UserDashboard/>}/>
        </Routes>
    </AnimatePresence>
  )
}

export default AnimatedRoutes