import {Row,Col,Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'
export default function Banner(){
    return(
        <div>
            <Row className='text-light'>
                <Col className='text-end mt-5 pt-5'>
                    <h1 className='dosis'>Shut In PH</h1>
                    <p className='pt-1'>We solemnly swear you'll never go out again</p>
                    <Button className='pt-1 mb-5' as={Link} to='/Services'> Check Services</Button>
                </Col>
            </Row>
        </div>
    )
}