import React, { useState } from "react";
import { Table,Button } from "react-bootstrap";
import { useEffect,useContext } from "react";
import useLoader from "../hooks/useLoader";
function ServiceTable({archiveService,editService}) {
    const [loader, showLoader, hideLoader] = useLoader();
    const [service,setService] = useState([])
    
    useEffect(()=>{
        showLoader();
        fetch(`${process.env.REACT_APP_API_URL}/service/getActiveService`,{
          headers:{
            'Content-Type': 'application/json'
          }
        }) 
        .then(result => result.json())
        .then(data =>{
          setService(data)   
          hideLoader()
          }
        )       
    },[])
    function refreshService(event){
      event.preventDefault();
      showLoader();
        fetch(`${process.env.REACT_APP_API_URL}/service/getActiveService`,{
          headers:{
            'Content-Type': 'application/json'
          }
        }) 
        .then(result => result.json())
        .then(data =>{
          setService(data)   
          hideLoader();
          }
        )  
    }
  return (
    <div className="pb-5">
    {loader}
      <Button className="px-3 col-1 offset-11" onClick={(event)=>{refreshService(event)}}>Refresh</Button>
      <Table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Description</th>
                <th>Image</th>
                <th>Created on</th>                         
            </tr>
        </thead>
        {service.map(services => (
          <tbody key={services._id} >
            <tr>
                <td>{services.name}</td>
                <td>{services.price}</td>
                <td>{services.description}</td>
                <td>{services.image}</td>
                <td>{services.createdOn}</td>
                <td ><Button className="mx-auto px-auto" onClick={(event) => {editService(services)}}>Edit</Button></td>
                <td className="px-5"><Button className="mx-auto px-5" onClick={(event) => {archiveService(services._id)}}>Archive</Button></td>
            </tr>
          </tbody>
        ))}
        
      </Table>
    </div>
  );
}

export default ServiceTable;