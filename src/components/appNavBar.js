import { Fragment, useContext} from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link,NavLink, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Logo from './logo.svg'

export default function AppNavBar(){

  // const [user,setUser] = useState(localStorage.getItem('email'))
  const {user} = useContext(UserContext);
    return (
    <Navbar bg="dark" variant='dark' expand="lg">
      <Container>
        <Navbar.Brand as={Link} to={user && user.isAdmin?'/AdminDashboard': '/'}>
        <img
              src={Logo}
              width="30"
              height="30"
              className="d-inline-block align-top"
              alt="Shut IN logo"
            />{' '}Shut In</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            
            {
              user && user.isAdmin?

              <Nav.Link as ={NavLink} to="/AdminDashboard">Dashboard</Nav.Link>
              :
              user && !user.isAdmin?
              <Fragment>
                <Nav.Link as={NavLink} to ='/'>Home</Nav.Link>
                <Nav.Link as={NavLink} to ='Services'>Services</Nav.Link>
                <Nav.Link as={NavLink} to ='/UserDashboard'>Dashboard</Nav.Link>
              </Fragment>
              :
              null
            }
            {
              user?     
                  <Nav.Link as={NavLink} to ='Logout'>Logout</Nav.Link>
              :
              <Fragment>
                  <Nav.Link as={NavLink} to ='Register'>Register</Nav.Link>
                  <Nav.Link as={NavLink} to ='Login'>Login</Nav.Link>
              </Fragment>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    )
    
}