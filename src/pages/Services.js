import ServiceCarousel from '../components/ServiceCarousel'
import { useState,useContext } from 'react'
import UserContext from '../UserContext';
import {Container} from 'react-bootstrap';
import { motion } from 'framer-motion';
import useLoader from '../hooks/useLoader'

export default function Services(){
    const {user}= useContext(UserContext);
    const [loader, showLoader, hideLoader] = useLoader();
    return(
        <motion.div
            initial={{opacity:0}}
            animate={{opacity:1}}
            exit={{opacity:0}}
        >
            {loader}
            <h1 className='mt-3 text-center text-light my-2 py-5'>Services</h1>
            <Container fluid className='bg-light col-8 rounded'>
                <ServiceCarousel/>
            </Container>
        </motion.div>
        )
}