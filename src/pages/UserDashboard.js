import { Button, Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserTable from '../components/UserTable'
import useLoader from "../hooks/useLoader";
import Swal from "sweetalert2";
import { useParams } from "react-router-dom";
import { motion } from "framer-motion";
export default function UserDashboard(){
    const [loader, showLoader, hideLoader] = useLoader();
    const{serviceId} = useParams();
    function cancelBooking(serviceId){
        console.log('Cancel')
        showLoader();
              fetch(`${process.env.REACT_APP_API_URL}/user/cancelService/${serviceId}`,{
                  method: 'PUT',
                  headers:{
                      'Content-Type':'application/json',
                      Authorization:`Bearer ${localStorage.getItem('token')}`
                  }}).then(result => result.json())
                  .then(data =>{
                      if(data === false){
                          hideLoader();
                          Swal.fire({
                              title: "Failed!",
                              icon: "error",
                              text: "Please try again!"
                          })
                      }
                      else{
                          hideLoader();
                          Swal.fire({
                              title: "Service Cancelled Successfully!",
                              icon: 'success', 
                          })
                      }
                  })
                  .catch(error => console.log(error))
      }
    return(
         <motion.div
         initial={{opacity:0}}
            animate={{opacity:1}}
            exit={{opacity:0}}>
         {loader}
         <h1 className='mt-3 text-center text-light'>UserDashboard</h1>
            <Container fluid className="col-10 bg-light rounded">
                    <h4 className="col-xs-10 mx-auto px-2 text-center pt-3">Bookings</h4>
                <Row>
                <Col >
                       <UserTable cancelBooking={cancelBooking}/>
                </Col>
                </Row>
                <Row>
                    <Col className='col-2 mx-auto mb-5'>
                        <Button  as={Link} to='/Services'> Check More Services</Button>
                    </Col>
                </Row>
            </Container>
         </motion.div>
    )
}