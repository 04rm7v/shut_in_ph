import {Button,Container,Form,Row,Col} from 'react-bootstrap';
import {  useContext, useEffect, useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { motion } from 'framer-motion';
export default function Register(){
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [confirmPassword,setConfirmPassword] =useState("");
    const [fullname,setFullname] = useState('');
    const [address,setAddress] = useState(``);
    const [number,setNumber] = useState('');
    const [isActive,setIsActive] = useState(false)
    const {user,setUser} = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(()=> {
        if(email !== "" &&password !== "" && confirmPassword !== "" && password === confirmPassword){
            setIsActive(true)
        }
        else{
            setIsActive(false)
        }
    },[email,password,confirmPassword]);
    function register(event){
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/user/register`,{
            method: 'POST',
            headers:{
                'Content-Type' : 'application/json'
            },
            body:JSON.stringify({
                fullname : fullname,
                address :address,
                mobileNo:number,
                email : email,
                password : password
            })
        }).then(result =>{
            result.json()
        })
        .then(data =>{

            if(data === false){
                Swal.fire({
                    title: "Registration failed!",
                    icon: "error",
                    text: "Please try again!"
                })
            }
            else{
                Swal.fire({
                    title: "Registration successful!",
                    icon: 'success',
                    text: "Welcome to Zuitt!"    
                })
                navigate('/Login');
            }
           
            
        })

    }
    return(
        user?
        <Navigate to ='/*'/>
        :
        <motion.div
            initial={{opacity:0}}
            animate={{opacity:1}}
            exit={{opacity:0}}
        >
        <Container >
            <Row>
                <Col className='col-xs-10 col-sm-4 ms-auto text-light mt-4'>
                <Form className='col-10 mx-auto' onSubmit={event => register(event)}>
                        <h1 className='text-center'>Register</h1>
                    <Form.Group className="mb-3" controlId="formBasicFullName">
                            <Form.Label>Full Name</Form.Label>
                            <Form.Control 
                            type="text" 
                            placeholder="Enter Full Name"
                            value={fullname}
                            onChange={event => setFullname(event.target.value)} 
                            required
                            />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                            type="email" 
                            placeholder="Enter email"
                            value={email}
                            onChange={event => setEmail(event.target.value)} 
                            required
                            />
                            <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicAddress">
                            <Form.Label>Address</Form.Label>
                            <Form.Control 
                            type="text" 
                            placeholder="Enter Address"
                            value={address}
                            onChange={event => setAddress(event.target.value)} 
                            required
                            />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicNumber">
                            <Form.Label>Phone Number</Form.Label>
                            <Form.Control 
                            type="number" 
                            placeholder="Enter Phone Number"
                            value={number}
                            onChange={event => setNumber(event.target.value)} 
                            required
                            />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={password}
                        onChange={event => setPassword(event.target.value)} 
                        required />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={confirmPassword}
                        onChange={event => setConfirmPassword(event.target.value)} 
                        required
                        />
                    </Form.Group>
                    {
                        isActive ?
                        <Button variant="primary" type="submit">
                        Submit
                        </Button>
                        :
                        <Button variant="danger" type="submit" disabled>
                        Submit
                        </Button>
                    }
                    
                    </Form>
                </Col>
            </Row>
            </Container>
        </motion.div>
        
    )
}